package com.dubinin.task.controller.processor.impl;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.dubinin.task.controller.handler.HandlerManager;
import com.dubinin.task.controller.service.DataIteratorService;
import com.dubinin.task.view.DataWriter;

@RunWith(MockitoJUnitRunner.class)
public class DataProcessorImplTest {

    private static final String TEST_CATEGORY_1 = "test_category_1";
    private static final String TEST_CATEGORY_2 = "test_category_2";
    private static final String TEST_ITEM_1 = "test_item_1";
    private static final String TEST_ITEM_2 = "test_item_2";

    private DataProcessorImpl dataProcessor;
    @Mock
    private DataIteratorService dataIteratorServiceMock;
    @Mock
    private DataWriter dataWriterMock;
    @Mock
    private HandlerManager<String> handlerManagerMock;

    @Before
    public void setUp() {
        dataProcessor = new DataProcessorImpl(dataWriterMock, handlerManagerMock);
    }

    @Test
    public void shouldNotBeHandledItemWhenLineIsEmpty() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, false);
        when(dataIteratorServiceMock.next()).thenReturn("");

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).hasHandler(anyString());
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenItemIsEmpty() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, "");
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(true);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenCategoryIsEmpty() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn("", TEST_ITEM_1);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenCategoryIsValidAndItemIsCategory() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, TEST_CATEGORY_1);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(true);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenCategoryIsNotValid() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, TEST_ITEM_1);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(false);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenCategoryIsSpaceSymbol() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(" ", TEST_ITEM_1);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldNotBeHandledItemWhenItemIsSpaceSymbol() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, " ");

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock, never()).executeHandler(anyString(), anyString());
    }

    @Test
    public void shouldBeHandledItemWhenCategoryIsValidAndItemIsNotEmpty() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, TEST_ITEM_1);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(true);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock).executeHandler(eq(TEST_CATEGORY_1), eq(TEST_ITEM_1));
    }

    @Test
    public void shouldBeHandledTwoItemsWithCommonCategory() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, TEST_ITEM_1, TEST_ITEM_2);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(true);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock).executeHandler(eq(TEST_CATEGORY_1), eq(TEST_ITEM_1));
        verify(handlerManagerMock).executeHandler(eq(TEST_CATEGORY_1), eq(TEST_ITEM_2));
    }

    @Test
    public void shouldBeHandledTwoItemsWithDifferentCategories() throws IOException {
        when(dataIteratorServiceMock.hasNext()).thenReturn(true, true, true, true, false);
        when(dataIteratorServiceMock.next()).thenReturn(TEST_CATEGORY_1, TEST_ITEM_1, TEST_CATEGORY_2, TEST_ITEM_2);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_1))).thenReturn(true);
        when(handlerManagerMock.hasHandler(eq(TEST_CATEGORY_2))).thenReturn(true);

        dataProcessor.process(dataIteratorServiceMock);
        verify(handlerManagerMock).executeHandler(eq(TEST_CATEGORY_1), eq(TEST_ITEM_1));
        verify(handlerManagerMock).executeHandler(eq(TEST_CATEGORY_2), eq(TEST_ITEM_2));
    }
}
