package com.dubinin.task.controller.service.impl;

import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.dubinin.task.repository.impl.FileIteratorRepositoryImpl;

@RunWith(MockitoJUnitRunner.class)
public class DataIteratorServiceImplTest {
    private DataIteratorServiceImpl dataIteratorService;

    @Mock
    private FileIteratorRepositoryImpl fileIteratorRepositoryMock;

    @Before
    public void setUp() {
        dataIteratorService = new DataIteratorServiceImpl(fileIteratorRepositoryMock);
    }

    @Test
    public void shouldBeCalledHasNext() {
        dataIteratorService.hasNext();

        verify(fileIteratorRepositoryMock).hasNext();
    }

    @Test
    public void shouldBeCalledNext() {
        dataIteratorService.next();

        verify(fileIteratorRepositoryMock).next();
    }

    @Test
    public void shouldBeCalledClose() throws IOException {
        dataIteratorService.close();

        verify(fileIteratorRepositoryMock).close();
    }
}
