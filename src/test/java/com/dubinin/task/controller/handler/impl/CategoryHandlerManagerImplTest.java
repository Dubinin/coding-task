package com.dubinin.task.controller.handler.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.dubinin.task.controller.handler.Handler;
import com.dubinin.task.controller.handler.HandlerManager;
import com.dubinin.task.controller.handler.HandlerManagerContext;
import com.dubinin.task.entity.HandlerResult;

@RunWith(MockitoJUnitRunner.class)
public class CategoryHandlerManagerImplTest {
    private static final String TEST_HANDLER_NAME = "test_handler";
    private static final String TEST_LINE = "test_line";
    private static final String INCORRECT_HANDLER_NAME = "incorrect_handler_name";

    private HandlerManager<String> handlerManager;

    @Mock
    private Handler handlerMock;
    @Mock
    private HandlerManagerContext handlerManagerContextMock;

    @Before
    public void setUp() {
        handlerManager = new CategoryHandlerManagerImpl(handlerManagerContextMock);
    }

    @Test
    public void shouldBeRegisteredHandler() {
        when(handlerManagerContextMock.hasHandlers(TEST_HANDLER_NAME)).thenReturn(true);

        assertTrue(handlerManager.hasHandler(TEST_HANDLER_NAME));
    }

    @Test
    public void shouldBeExecuteHandlerWhenHandlerWasRegistered() {
        when(handlerManagerContextMock.hasHandlers(TEST_HANDLER_NAME)).thenReturn(true);
        when(handlerManagerContextMock.getHandlers(TEST_HANDLER_NAME)).thenReturn(Collections.singletonList(handlerMock));

        handlerManager.executeHandler(TEST_HANDLER_NAME, TEST_LINE);

        verify(handlerMock).handle(eq(TEST_LINE));
    }

    @Test
    public void shouldBeExecuteTwoHandlersWhenHandlersWasRegisteredForCommonCategory() {
        List<Handler> handlers = new ArrayList<>();
        handlers.add(handlerMock);
        handlers.add(handlerMock);

        when(handlerManagerContextMock.hasHandlers(TEST_HANDLER_NAME)).thenReturn(true);
        when(handlerManagerContextMock.getHandlers(TEST_HANDLER_NAME)).thenReturn(handlers);

        handlerManager.executeHandler(TEST_HANDLER_NAME, TEST_LINE);

        verify(handlerMock, times(2)).handle(eq(TEST_LINE));
    }

    @Test
    public void shouldBeReturnDataWhenHandlerIsExist() {

        when(handlerMock.getData()).thenReturn(Collections.singletonList(TEST_LINE));
        when(handlerManagerContextMock.hasHandlers(TEST_HANDLER_NAME)).thenReturn(true);
        when(handlerManagerContextMock.getHandlers(TEST_HANDLER_NAME)).thenReturn(Collections.singletonList(handlerMock));
        when(handlerManagerContextMock.getAllHandlers())
                .thenReturn(Collections.singletonMap(TEST_HANDLER_NAME, Collections.singletonList(handlerMock)));

        handlerManager.executeHandler(TEST_HANDLER_NAME, TEST_LINE);

        List<HandlerResult> result = handlerManager.getHandlerResults();

        assertFalse(result.isEmpty());
        assertEquals(TEST_LINE, result.get(0).getData().get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenHandlerNameIsIncorrect() {
        handlerManager.executeHandler(INCORRECT_HANDLER_NAME, TEST_LINE);
    }
}
