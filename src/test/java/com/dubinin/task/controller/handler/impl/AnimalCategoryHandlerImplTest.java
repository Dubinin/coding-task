package com.dubinin.task.controller.handler.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class AnimalCategoryHandlerImplTest {
    private static final String ITEM = "item";
    private static final String ITEM_2 = "item2";
    private static final String ITEM_3 = "item3";
    private AnimalCategoryHandlerImpl handler;

    @Before
    public void setUp() {
        handler = new AnimalCategoryHandlerImpl();
    }

    @Test
    public void shouldReturnOneItemWhenAddedTwoIdenticalButInDifferentCase() {
        handler.handle(ITEM);
        handler.handle(ITEM.toLowerCase());

        List<String> result = handler.getData();

        assertEquals(1, result.size());
    }

    @Test
    public void shouldReturnSortedItems() {
        handler.handle(ITEM_2);
        handler.handle(ITEM);
        handler.handle(ITEM_3);

        List<String> result = handler.getData();

        assertEquals(3, result.size());
        assertEquals(ITEM, result.get(0));
        assertEquals(ITEM_2, result.get(1));
        assertEquals(ITEM_3, result.get(2));
    }
}
