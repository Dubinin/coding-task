package com.dubinin.task.controller.handler.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class CarCategoryHandlerImplTest {
    private static final String ITEM = "bmw";
    private static final String ITEM_2 = "opel";
    private static final String ITEM_3 = "vw";

    private static final String ITEM_WITH_MD_5 = "bmw (71913f59e458e026d6609cdb5a7cc53d)";
    private static final String ITEM_2_WITH_MD_5 = "opel (f65b7d39472c52142ea2f4ea5e115d59)";
    private static final String ITEM_3_WITH_MD_5 = "vw (7336a2c49b0045fa1340bf899f785e70)";

    private CarCategoryHandlerImpl handler;

    @Before
    public void setUp() {
        handler = new CarCategoryHandlerImpl();
    }

    @Test
    public void shouldReturnOneItemWhenAddedTwoIdenticalButInDifferentCase() {
        handler.handle(ITEM);
        handler.handle(ITEM.toLowerCase());

        List<String> result = handler.getData();

        assertEquals(1, result.size());
    }

    @Test
    public void shouldReturnItemWithMd5() {
        handler.handle(ITEM);

        List<String> result = handler.getData();

        assertEquals(1, result.size());
        assertEquals(ITEM_WITH_MD_5, result.get(0));
    }

    @Test
    public void shouldReturnSortedItems() {
        handler.handle(ITEM_2);
        handler.handle(ITEM);
        handler.handle(ITEM_3);

        List<String> result = handler.getData();

        assertEquals(3, result.size());
        assertEquals(ITEM_3_WITH_MD_5, result.get(0));
        assertEquals(ITEM_2_WITH_MD_5, result.get(1));
        assertEquals(ITEM_WITH_MD_5, result.get(2));
    }
}
