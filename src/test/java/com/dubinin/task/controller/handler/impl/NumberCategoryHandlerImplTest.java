package com.dubinin.task.controller.handler.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class NumberCategoryHandlerImplTest {
    private static final String ITEM_1 = "1";
    private static final String ITEM_2 = "2";
    private static final String ITEM_3 = "3";

    private static final String ITEM_1_NUMBER_1 = "1 : 1";
    private static final String ITEM_2_NUMBER_1 = "2 : 1";
    private static final String ITEM_3_NUMBER_1 = "3 : 1";

    private static final String ITEM_1_NUMBER_2 = "1 : 2";
    private static final String ITEM_3_NUMBER_2 = "3 : 2";

    private NumberCategoryHandlerImpl handler;

    @Before
    public void setUp() {
        handler = new NumberCategoryHandlerImpl();
    }

    @Test
    public void shouldReturnOneItemWhenAddedTwoIdenticalButInDifferentCase() {
        handler.handle(ITEM_1);
        handler.handle(ITEM_1.toLowerCase());

        List<String> result = handler.getData();

        assertEquals(1, result.size());
    }

    @Test
    public void shouldReturnNumbersWithNumberOfDuplicates() {
        handler.handle(ITEM_1);
        handler.handle(ITEM_1);
        handler.handle(ITEM_2);
        handler.handle(ITEM_3);
        handler.handle(ITEM_3);

        List<String> result = handler.getData();

        assertEquals(3, result.size());
        assertEquals(ITEM_1_NUMBER_2, result.get(0));
        assertEquals(ITEM_2_NUMBER_1, result.get(1));
        assertEquals(ITEM_3_NUMBER_2, result.get(2));
    }

    @Test
    public void shouldReturnSortedItems() {
        handler.handle(ITEM_2);
        handler.handle(ITEM_1);
        handler.handle(ITEM_3);

        List<String> result = handler.getData();

        assertEquals(3, result.size());
        assertEquals(ITEM_1_NUMBER_1, result.get(0));
        assertEquals(ITEM_2_NUMBER_1, result.get(1));
        assertEquals(ITEM_3_NUMBER_1, result.get(2));
    }
}
