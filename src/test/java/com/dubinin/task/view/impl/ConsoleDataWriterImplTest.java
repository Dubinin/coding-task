package com.dubinin.task.view.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConsoleDataWriterImplTest {
    private static final String HANDLER_NAME = "test_handler_name";
    private static final String HANDLER_RESULT = "test_handler_result";

    private ConsoleDataWriterImpl consoleDataWriter;
    private final PrintStream originalOut = System.out;
    private ByteArrayOutputStream outContent;

    @Before
    public void setUp() {
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        consoleDataWriter = new ConsoleDataWriterImpl();
    }

    @After
    public void destroy() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldNotPrintMessageWhenHandlerResultIsEmpty() {
        consoleDataWriter.write(HANDLER_NAME, Collections.emptyList());

        assertEquals("", outContent.toString());
    }

    @Test
    public void shouldBeContainHandlerNameInUpperCase() {
        consoleDataWriter.write(HANDLER_NAME, Collections.singletonList(HANDLER_RESULT));
        assertTrue(outContent.toString().contains(HANDLER_NAME.toUpperCase()));
    }

    @Test
    public void shouldBeContainHandlerData() {
        consoleDataWriter.write(HANDLER_NAME, Collections.singletonList(HANDLER_RESULT));
        assertTrue(outContent.toString().contains(HANDLER_RESULT));
    }

    @Test
    public void shouldBeContainAsteriskLineSeparator() {
        consoleDataWriter.write(HANDLER_NAME, Collections.singletonList(HANDLER_RESULT));
        assertTrue(outContent.toString().contains(System.lineSeparator() + "*"));
    }

    @Test
    public void shouldPrintMessageWhenHandlerResultIsNotEmpty() {
        consoleDataWriter.write(HANDLER_NAME, Collections.singletonList(HANDLER_RESULT));
        assertEquals(HANDLER_NAME.toUpperCase() + System.lineSeparator() + "* " +
                HANDLER_RESULT + System.lineSeparator(), outContent.toString());
    }
}
