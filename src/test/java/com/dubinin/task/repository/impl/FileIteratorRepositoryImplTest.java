package com.dubinin.task.repository.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.Test;

public class FileIteratorRepositoryImplTest {
    private static final String FULL_FILE_PATH =
            FileIteratorRepositoryImplTest.class.getClassLoader().getResource("full-file.txt").getPath();
    private static final String EMPTY_FILE_PATH =
            FileIteratorRepositoryImplTest.class.getClassLoader().getResource("empty-file.txt").getPath();
    private static final String TEST_LINE = "test-line";

    private FileIteratorRepositoryImpl fileIteratorRepository;

    @After
    public void destroy() {
        fileIteratorRepository.close();
    }

    @Test
    public void shouldReturnTrueWhenFileIsNotEmpty() throws IOException {
        fileIteratorRepository = new FileIteratorRepositoryImpl(FULL_FILE_PATH);
        assertTrue(fileIteratorRepository.hasNext());
    }

    @Test
    public void shouldReturnFalseWhenFileIsEmpty() throws IOException {
        fileIteratorRepository = new FileIteratorRepositoryImpl(EMPTY_FILE_PATH);
        assertFalse(fileIteratorRepository.hasNext());
    }

    @Test
    public void shouldReturnLineWhenFileIsNotEmpty() throws IOException {
        fileIteratorRepository = new FileIteratorRepositoryImpl(FULL_FILE_PATH);
        assertTrue(fileIteratorRepository.hasNext());
        assertEquals(TEST_LINE, fileIteratorRepository.next());
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldThrowExceptionWhenFileIsEmptyAndWasCalledNextFunction() throws IOException {
        fileIteratorRepository = new FileIteratorRepositoryImpl(EMPTY_FILE_PATH);
        fileIteratorRepository.next();
    }
}
