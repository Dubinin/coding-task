package com.dubinin.task.repository;

import java.io.Closeable;
import java.util.Iterator;

/**
 * Interface to provide data line by line.
 */
public interface IteratorRepository extends Iterator<String>, Closeable {
}
