package com.dubinin.task.repository.impl;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import com.dubinin.task.repository.IteratorRepository;

/**
 * Class for providing data from file line by line.
 */
public class FileIteratorRepositoryImpl implements IteratorRepository {
    private LineIterator lineIterator;

    public FileIteratorRepositoryImpl(String path) throws IOException {
        this.lineIterator = FileUtils.lineIterator(new File(path));
    }

    /**
     * Returns {@code true} if the file has more line.
     * @return {@code true} if the file has more line
     */
    @Override
    public boolean hasNext() {
        return lineIterator.hasNext();
    }

    /**
     * Returns the next line in the file.
     * @throws NoSuchElementException if the file has no more line
     * @return the next line in the file
     */
    @Override
    public String next() {
        return lineIterator.nextLine();
    }

    /**
     * Closes the connection to the file.
     */
    @Override
    public void close() {
        lineIterator.close();
    }
}
