package com.dubinin.task.view.impl;

import java.util.List;
import java.util.Optional;

import com.dubinin.task.view.DataWriter;

/**
 * Class to format and output processed data.
 */
public class ConsoleDataWriterImpl implements DataWriter {
    private static final String DATA_ITEM_FORMAT = "* %s";

    @Override
    public void write(final String dataName, final List<String> data) {
        Optional.of(data)
                .filter(results -> !results.isEmpty())
                .ifPresent(list -> {
                    System.out.println(dataName.toUpperCase());
                    list.stream()
                            .map(item -> String.format(DATA_ITEM_FORMAT, item))
                            .forEach(System.out::println);
                });
    }
}
