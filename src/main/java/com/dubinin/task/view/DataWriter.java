package com.dubinin.task.view;

import java.util.List;

/**
 * Interface to write processed data.
 */
public interface DataWriter {
    /**
     * Write processed data.
     * @param dataName name of data
     * @param data data items
     */
    void write(final String dataName, final List<String> data);
}
