package com.dubinin.task.controller.service.impl;

import java.io.IOException;

import com.dubinin.task.repository.IteratorRepository;
import com.dubinin.task.controller.service.DataIteratorService;

/**
 * Class to provide data access line by line.
 */
public class DataIteratorServiceImpl implements DataIteratorService {
    private IteratorRepository iteratorRepository;

    public DataIteratorServiceImpl(IteratorRepository iteratorRepository) {
        this.iteratorRepository = iteratorRepository;
    }

    @Override
    public boolean hasNext() {
        return iteratorRepository.hasNext();
    }

    @Override
    public String next() {
        return iteratorRepository.next();
    }

    @Override
    public void close() throws IOException {
        iteratorRepository.close();
    }
}
