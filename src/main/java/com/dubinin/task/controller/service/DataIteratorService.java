package com.dubinin.task.controller.service;

import java.io.Closeable;
import java.util.NoSuchElementException;

/**
 * Iterator wrapper interface.
 */
public interface DataIteratorService extends Closeable {

    /**
     * Returns {@code true} if the source has more line.
     * @return {@code true} if the source has more line
     */
    boolean hasNext();

    /**
     * /**
     * Returns the next line in the source.
     * @throws NoSuchElementException if the source has no more line
     * @return the next line in the source
     */
    String next();
}
