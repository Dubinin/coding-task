package com.dubinin.task.controller.processor;

import java.io.IOException;

import com.dubinin.task.controller.service.DataIteratorService;

/**
 * Interface for line-by-line data processing.
 */
public interface DataProcessor {

    /**
     * Method to data processing.
     *
     * @param dataIteratorService data source.
     * @throws IOException if an I/O error occurs
     */
    void process(final DataIteratorService dataIteratorService) throws IOException;
}
