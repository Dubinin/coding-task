package com.dubinin.task.controller.processor.impl;

import java.io.IOException;
import java.util.Optional;

import com.dubinin.task.controller.handler.HandlerManager;
import com.dubinin.task.controller.processor.DataProcessor;
import com.dubinin.task.controller.service.DataIteratorService;
import com.dubinin.task.view.DataWriter;

public class DataProcessorImpl implements DataProcessor {
    private DataWriter categoryView;
    private HandlerManager<String> handlerManager;

    public DataProcessorImpl(DataWriter view, HandlerManager<String> handlerManager) {
        this.handlerManager = handlerManager;
        this.categoryView = view;
    }

    public void process(final DataIteratorService dataIteratorService) throws IOException {
        try {
            executeHandlers(dataIteratorService);
        } finally {
            dataIteratorService.close();
        }

        provideResults();
    }

    /**
     * Method for providing the results of processing to the view.
     */
    private void provideResults() {
        handlerManager.getHandlerResults().forEach((result) -> categoryView.write(result.getName(), result.getData()));
    }

    /**
     * Method to perform data processing from source.
     *
     * @param dataIteratorService data source
     */
    private void executeHandlers(final DataIteratorService dataIteratorService) {
        String currentCategoryName = null;

        while (dataIteratorService.hasNext()) {
            String line = normalize(dataIteratorService.next());
            currentCategoryName = findCategoryName(line, currentCategoryName);

            Optional.ofNullable(currentCategoryName)
                    .ifPresent(categoryName ->
                            Optional.of(line)
                                    .filter(li -> li.length() > 0)
                                    .filter(li -> !isCategory(li))
                                    .ifPresent(li -> handlerManager.executeHandler(categoryName, li)));
        }
    }

    private String findCategoryName(final String line, final String currentCategoryName) {
        return Optional.of(line).filter(li -> li.length() > 0).filter(this::isCategory).orElse(currentCategoryName);
    }

    private boolean isCategory(final String line) {
        return handlerManager.hasHandler(line);
    }

    private String normalize(final String line) {
        return line.trim().toLowerCase();
    }
}
