package com.dubinin.task.controller.handler.impl;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import com.dubinin.task.controller.handler.Handler;
import com.dubinin.task.controller.handler.HandlerManager;
import com.dubinin.task.controller.handler.HandlerManagerContext;
import com.dubinin.task.entity.HandlerResult;
import com.google.common.base.Preconditions;

/**
 * Handler manager class, that allows basic management operations with handlers.  Allows to obtain handlers execution results.
 */
public class CategoryHandlerManagerImpl implements HandlerManager<String> {
    private static final String ERROR_MSG = "Incorrect handler name [%s].";
    private HandlerManagerContext handlerContext;

    public CategoryHandlerManagerImpl(HandlerManagerContext handlerContext) {
        this.handlerContext = handlerContext;
    }

    @Override
    public void executeHandler(final String name, final String data) {
        Preconditions.checkArgument(handlerContext.hasHandlers(name), ERROR_MSG, name);
        handlerContext.getHandlers(name).forEach(handler -> handler.handle(data));
    }

    @Override
    public boolean hasHandler(final String name) {
        return handlerContext.hasHandlers(name);
    }

    @Override
    public List<HandlerResult> getHandlerResults() {
        final List<HandlerResult> handlerResults = new ArrayList<>();
        handlerContext.getAllHandlers().forEach((key, value) -> handlerResults.addAll(extractResults(key, value)));
        return handlerResults;
    }

    private List<HandlerResult> extractResults(final String category, final List<Handler> handlers) {
        return handlers.stream().map(handler -> new HandlerResult(category, handler.getData())).collect(toList());
    }

}
