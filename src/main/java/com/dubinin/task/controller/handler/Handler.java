package com.dubinin.task.controller.handler;

import java.util.List;

/**
 * Interface for processing and providing the result of processing.
 */
public interface Handler {
    /**
     * Method providing the result of processing.
     *
     * @return result of processing
     */
    List<String> getData();

    /**
     * Method for data processing
     *
     * @param data data
     */
    void handle(final String data);
}
