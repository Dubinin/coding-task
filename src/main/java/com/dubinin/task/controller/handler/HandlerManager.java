package com.dubinin.task.controller.handler;

import java.util.List;

import com.dubinin.task.entity.HandlerResult;

/**
 * Interface for applying handlers to data and providing the result of their processing.
 *
 * @param <T> type of processing data
 */
public interface HandlerManager<T> {

    /**
     * Method for data processing.
     *
     * @param name name of handler
     * @param data process data
     */
    void executeHandler(final String name, final T data);

    /**
     * Returns {@code true} if HandlerManager has handler by specified name.
     *
     * @param name name of handler
     * @return Returns {@code true} if HandlerManager has handler by specified name
     */
    boolean hasHandler(final String name);

    /**
     * The method provides the result of data processing.
     *
     * @return result of data processing
     */
    List<HandlerResult> getHandlerResults();
}
