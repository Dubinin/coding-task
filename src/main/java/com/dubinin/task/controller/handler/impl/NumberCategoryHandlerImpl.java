package com.dubinin.task.controller.handler.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.dubinin.task.controller.handler.Handler;

/**
 * Class for processing number category data and providing the result of processing.
 */
public class NumberCategoryHandlerImpl implements Handler {
    private static final String DATA_LINE_FORMAT = "%s : %s";
    private final Map<String, Integer> numbers = new HashMap<>();

    @Override
    public void handle(final String data) {
        numbers.merge(data, 1, Integer::sum);
    }

    @Override
    public List<String> getData() {
        return numbers.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .map(entry -> String.format(DATA_LINE_FORMAT, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
