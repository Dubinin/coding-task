package com.dubinin.task.controller.handler.impl;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.dubinin.task.controller.handler.Handler;

/**
 * Class for processing animal category data and providing the result of processing
 */
public class AnimalCategoryHandlerImpl implements Handler {
    private final Set<String> animals = new LinkedHashSet<>();

    @Override
    public void handle(final String data) {
        animals.add(data);
    }

    @Override
    public List<String> getData() {
        return animals.stream().sorted().collect(Collectors.toList());
    }
}
