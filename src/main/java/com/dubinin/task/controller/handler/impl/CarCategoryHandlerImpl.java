package com.dubinin.task.controller.handler.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;

import com.dubinin.task.controller.handler.Handler;

/**
 * Class for processing car category data and providing the result of processing
 */
public class CarCategoryHandlerImpl implements Handler {
    private static final String DATA_LINE_FORMAT = "%s (%s)";
    private final Map<String, String> cars = new HashMap<>();

    @Override
    public void handle(final String data) {
        cars.putIfAbsent(data, DigestUtils.md5Hex(data));
    }

    @Override
    public List<String> getData() {
        return cars.entrySet().stream()
                .sorted((o1, o2) -> o2.getKey().compareTo(o1.getKey()))
                .map(entry -> String.format(DATA_LINE_FORMAT, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
