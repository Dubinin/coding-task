package com.dubinin.task.controller.handler;

import java.util.List;
import java.util.Map;

/**
 * Interface provides methods for manage context of handlers.
 */
public interface HandlerManagerContext {
    /**
     * Get handlers by handler category name.
     *
     * @param name handler category name
     * @return list of categories
     */
    List<Handler> getHandlers(final String name);

    /**
     * Returns {@code true} if context has handler by specified name.
     *
     * @param name name of handler
     * @return Returns {@code true} if context has handler by specified name
     */
    boolean hasHandlers(final String name);

    /**
     * Get all handlers from context.
     *
     * @return all handlers
     */
    Map<String, List<Handler>> getAllHandlers();
}
