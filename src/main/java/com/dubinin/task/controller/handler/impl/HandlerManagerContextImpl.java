package com.dubinin.task.controller.handler.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.dubinin.task.controller.handler.Handler;
import com.dubinin.task.controller.handler.HandlerManagerContext;
import com.dubinin.task.exception.HandlerInstantiationException;
import com.dubinin.task.exception.HandlerManagerContextInitializationException;

public class HandlerManagerContextImpl implements HandlerManagerContext {
    private static final String CONFIGURATION_FILE_NAME = "handler-context-configuration.json";
    private static final String HANDLERS_PROPERTY = "handlers";
    private static final String CATEGORY_PROPERTY = "category";
    private static final String CLASS_PROPERTY = "class";
    private static final String CREATING_HANDLER_ERROR =
            "Error of creating the handler object according to the following class [%s]";
    private static final String READING_CONFIG_ERROR = "Error of reading the handler configuration file by path [%s]";
    private static final String PARSING_CONFIG_ERROR = "Error of parsing the handler configuration file by path [%s]";
    private static final JSONParser JSON_PARSER = new JSONParser();
    private final Map<String, List<Handler>> context = new HashMap<>();

    public HandlerManagerContextImpl() {
        initContext();
    }

    private void initContext() {
        final JSONArray handlerList = getHandlerConfiguration(CONFIGURATION_FILE_NAME);
        for (Object aHandlerList : handlerList) {
            final JSONObject handlerConfig = (JSONObject) aHandlerList;
            register((String) handlerConfig.get(CATEGORY_PROPERTY),
                    provideHandler((String) handlerConfig.get(CLASS_PROPERTY)));
        }
    }

    private JSONArray getHandlerConfiguration(final String configPath) {
        try {
            final ClassLoader loader = Thread.currentThread().getContextClassLoader();
            final JSONObject configuration =
                    (JSONObject) JSON_PARSER.parse(new InputStreamReader(loader.getResourceAsStream(configPath)));
            return (JSONArray) configuration.get(HANDLERS_PROPERTY);
        } catch (ParseException ex) {
            throw new HandlerManagerContextInitializationException(String.format(PARSING_CONFIG_ERROR, configPath), ex);
        } catch (IOException ex) {
            throw new HandlerManagerContextInitializationException(String.format(READING_CONFIG_ERROR, configPath), ex);
        }
    }

    private Handler provideHandler(final String handlerName) {
        try {
            final Class<?> clazz = Class.forName(handlerName);
            return (Handler) clazz.newInstance();
        } catch (ReflectiveOperationException ex) {
            throw new HandlerInstantiationException(String.format(CREATING_HANDLER_ERROR, handlerName), ex);
        }
    }

    @Override
    public List<Handler> getHandlers(String category) {
        return context.get(category);
    }

    @Override
    public boolean hasHandlers(String category) {
        return context.containsKey(category);
    }

    @Override
    public Map<String, List<Handler>> getAllHandlers() {
        return context;
    }

    private void register(final String category, final Handler handler) {
        context.merge(category, Collections.singletonList(handler),
                (handlers, newHandler) -> Stream.concat(handlers.stream(), newHandler.stream())
                        .collect(Collectors.toList()));
    }
}

