package com.dubinin.task;

import java.io.IOException;

import com.dubinin.task.controller.handler.HandlerManager;
import com.dubinin.task.controller.handler.HandlerManagerContext;
import com.dubinin.task.controller.handler.impl.CategoryHandlerManagerImpl;
import com.dubinin.task.controller.handler.impl.HandlerManagerContextImpl;
import com.dubinin.task.controller.processor.impl.DataProcessorImpl;
import com.dubinin.task.controller.service.DataIteratorService;
import com.dubinin.task.controller.service.impl.DataIteratorServiceImpl;
import com.dubinin.task.repository.impl.FileIteratorRepositoryImpl;
import com.dubinin.task.view.impl.ConsoleDataWriterImpl;
import com.google.common.base.Preconditions;

public class RunMe {

    private static final String MISSING_PARAM_ERROR_MESSAGE = "The path parameter is missing.";

    public static void main(String[] args) throws IOException {
        Preconditions.checkArgument(args.length > 0, MISSING_PARAM_ERROR_MESSAGE);

        final String pathToFile = args[0];

        final HandlerManagerContext handlerContext = new HandlerManagerContextImpl();
        final HandlerManager<String> handlerManager = new CategoryHandlerManagerImpl(handlerContext);
        final DataIteratorService dataIterator = new DataIteratorServiceImpl(new FileIteratorRepositoryImpl(pathToFile));
        new DataProcessorImpl(new ConsoleDataWriterImpl(), handlerManager).process(dataIterator);
    }
}
