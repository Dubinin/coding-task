package com.dubinin.task.entity;

import java.util.List;
import java.util.Objects;

/**
 * Handler result entity.
 */
public class HandlerResult {
    private String name;
    private List<String> data;

    public HandlerResult(String name, List<String> data) {
        this.name = name;
        this.data = data;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HandlerResult that = (HandlerResult) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, data);
    }

    @Override
    public String toString() {
        return "HandlerResult{" +
                "name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}
