package com.dubinin.task.exception;

/**
 * Exception as result of unsuccessful initialization of context in HandlerManager
 */
public class HandlerManagerContextInitializationException extends RuntimeException {
    public HandlerManagerContextInitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
