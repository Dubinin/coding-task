package com.dubinin.task.exception;

/**
 * Exception as result of unsuccessful instantiation of Handler
 */
public class HandlerInstantiationException extends RuntimeException {

    public HandlerInstantiationException(String message, Throwable cause) {
        super(message, cause);
    }
}
