# coding-task
The program provides the ability to process and structure data. <br />
A text file is used as the data source. The data from the files are read as optimally as possible and are provided in a convenient form on the console log.<br />
In this current, the program supports the processing of three categories ANIMALS, NUMBERS and CARS.<br />
The logic of processing each of the categories can be found below:<br />
    
* ANIMALS - finding unique data and provide it in sorted form.<br />
* NUMBERS - counting the number of repeating elements and displaying statistics about it.<br />
* CARS - finding unique data, performing a reverse sorting and giving their MD5 hash.<br />

The architecture of the program is designed in such a way that adding the ability to process new categories is as simple as possible.<br />

## How to build
To build project execute in the root directory the following command with Maven 3:
    
    mvn clean package
    
## How to run
To run aplication execute in the root/target directory following command:

    java -jar coding-task-1.0.jar [path to the input file]

## Input file information   
You can find the files in the following directory root/input-files
    
    input.txt - file with animals and numbers categories.
    input2.txt - file with animals, numbers and cars categories.
    input3.txt - file with all categories and invalid data (just for testing). 
    
    